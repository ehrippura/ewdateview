/*
 
 Copyright 2012 Tzu-Yi Lin
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 */

//
//  EWDateView.m
//  demo
//
//  Created by Tzu-Yi Lin on 12/6/1.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWDateView.h"
#import <CoreText/CoreText.h>

@interface EWDateView () {
    CFMutableAttributedStringRef dateString;
    CFMutableAttributedStringRef subString;
    CTFontRef subStringFont;
    CTFontRef dateFont;
}

@end

@implementation EWDateView

@synthesize type = _type;

- (void)createFont
{
    if (dateFont) CFRelease(dateFont);
    if (subStringFont) CFRelease(subStringFont);
    
    NSString *fontPath = [[NSBundle mainBundle] pathForResource:@"ARIBLK" ofType:@"TTF"];
    NSData *fontData = [NSData dataWithContentsOfFile:fontPath];
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)fontData);
    
    CGFontRef cgFont = CGFontCreateWithDataProvider(dataProvider);
    CGDataProviderRelease(dataProvider);
    
    subStringFont = CTFontCreateWithGraphicsFont(cgFont, 14, NULL, NULL);
    dateFont = CTFontCreateWithGraphicsFont(cgFont, 40, NULL, NULL);
    CGFontRelease(cgFont);
}

- (void)defaultConfig
{
    _color = [UIColor blackColor];
    self.backgroundColor = [UIColor whiteColor];
    self.type = EWDateViewTypeDate;
    subString = dateString = NULL;
    subStringFont = dateFont = NULL;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defaultConfig];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultConfig];
    }
    return self;
}

- (id)initWithType:(EWDateViewType)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)dealloc
{
    [_date release];
    [_color release];
    
    if (dateString) CFRelease(dateString);
    if (subString) CFRelease(subString);
    if (dateFont) CFRelease(dateString);
    if (subStringFont) CFRelease(subString);
    
    [super dealloc];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    // invert horizental
    CGContextConcatCTM(ctx, CGAffineTransformScale(CGAffineTransformMakeTranslation(0, self.bounds.size.height), 1.f, -1.f));
    
    CTTypesetterRef dateTypesetter = CTTypesetterCreateWithAttributedString(dateString);
    CTLineRef dateLine = CTTypesetterCreateLine(dateTypesetter, CFRangeMake(0, CFAttributedStringGetLength(dateString)));
    float flush = 0.5; // center
    double offset = CTLineGetPenOffsetForFlush(dateLine, flush, CGRectGetWidth(rect));
    CGContextSetTextPosition(ctx, offset, 25);
    CTLineDraw(dateLine, ctx);
    
    CFRelease(dateTypesetter);
    CFRelease(dateLine);
    
    CTTypesetterRef monthTypesetter = CTTypesetterCreateWithAttributedString(subString);
    CTLineRef monthLine = CTTypesetterCreateLine(monthTypesetter, CFRangeMake(0, CFAttributedStringGetLength(subString)));
    offset = CTLineGetPenOffsetForFlush(monthLine, flush, CGRectGetWidth(rect));
    CGContextSetTextPosition(ctx, offset, 5);
    CTLineDraw(monthLine, ctx);
    
    CFRelease(monthTypesetter);
    CFRelease(monthLine);
}

- (void)setDate:(NSDate *)date
{
    if (!date) return;
    
    if (_date != date) {
        [_date release];
        _date = [date retain];
    }
    
    NSArray *stringArray;
    if (self.type == EWDateViewTypeDate) {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MMM,dd"];
        stringArray = [[df stringFromDate:date] componentsSeparatedByString:@","];
        [df release];
    } else {
        NSTimeInterval time = [date timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
        int remainInt = (int)(time / 86400);
        NSString *dateLeft = [NSString stringWithFormat:@"%d", (remainInt >= 0) ? remainInt : 0];
        stringArray = [NSArray arrayWithObjects:NSLocalizedString(@"days left", @"days left"), dateLeft, nil];
    }
    
    if (dateString) CFRelease(dateString);
    if (subString) CFRelease(subString);
    
    dateString = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
    subString = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
    CFAttributedStringReplaceString(subString, CFRangeMake(0, 0), (CFStringRef)[[stringArray objectAtIndex:0] uppercaseString]);
    CFAttributedStringReplaceString(dateString, CFRangeMake(0, 0), (CFStringRef)[stringArray objectAtIndex:1]);
    
    [self createFont];
    
    if (subStringFont && dateFont) {
        CFAttributedStringSetAttribute(subString,
                                       CFRangeMake(0, CFAttributedStringGetLength(subString)),
                                       kCTFontAttributeName,
                                       subStringFont);
        CFAttributedStringSetAttribute(dateString,
                                       CFRangeMake(0, CFAttributedStringGetLength(dateString)),
                                       kCTFontAttributeName,
                                       dateFont);
    }
    
    [self setColor:_color];
}

- (void)setColor:(UIColor *)color
{
    if (color != _color) {
        [_color release];
        _color = [color retain];
    }

    if (dateString && subString) {
        CGColorRef cgColor = [color CGColor];
        CFAttributedStringSetAttribute(dateString,
                                       CFRangeMake(0, CFAttributedStringGetLength(dateString)),
                                       kCTForegroundColorAttributeName,
                                       cgColor);
        CFAttributedStringSetAttribute(subString,
                                       CFRangeMake(0, CFAttributedStringGetLength(subString)),
                                       kCTForegroundColorAttributeName,
                                       cgColor);
        
        [self setNeedsDisplay];
    }
}

- (void)setType:(EWDateViewType)type
{
    _type = type;
    [self setDate:_date];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<EWDateView> with date: %@", _date];
}

@end

/*
 
 Copyright 2012 Tzu-Yi Lin
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 */

//
//  EWDateView.h
//  demo
//
//  Created by Tzi-Yi Lin on 12/6/1.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    EWDateViewTypeDate = 0,
    EWDateViewTypeCountdown = 1,
};
typedef NSInteger EWDateViewType;

@interface EWDateView : UIView {
    NSDate *_date;
    UIColor *_color;
    EWDateViewType _type;
}

@property (nonatomic, assign) EWDateViewType type;

- (id)initWithType:(EWDateViewType)type;
- (void)setDate:(NSDate *)date;
- (void)setColor:(UIColor *)color;

@end

//
//  main.m
//  demo
//
//  Created by 林 子頤 on 12/6/1.
//  Copyright (c) 2012年 Otouching Media. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EWAppDelegate class]));
    }
}

/*
 
 Copyright 2012 Tzu-Yi Lin
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 */


//
//  EWViewController.m
//  demo
//
//  Created by Tzu-Yi Lin on 12/6/1.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWViewController.h"
#import "EWDateView.h"
#import <QuartzCore/QuartzCore.h>

@interface EWViewController ()

@end

@implementation EWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    dateView.type = EWDateViewTypeCountdown;
    [dateView setDate:[NSDate dateWithTimeIntervalSinceNow:86500]];
    [dateView setColor:[UIColor colorWithRed:0.93f green:0.68f blue:0.19f alpha:1.00f]];
    dateView.backgroundColor = [UIColor grayColor];
    dateView.layer.masksToBounds = YES;
    [dateView.layer setCornerRadius:5.f];
    
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(test:) userInfo:nil repeats:NO];
}

- (void)test:(NSTimer *)timer
{
    dateView.type = EWDateViewTypeDate;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
